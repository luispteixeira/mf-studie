
define(function (require) {
    
  var fakeData = require('./models/fakeData4Model');
  var RoundGraphModel = require('./models/roundGraphModel');
  var RoundGraphView = require('./views/roundGraphView');
  //DOM elements
  var graphicsWarper = document.getElementById('graphic-warper');
  var graphicsTemplate = document.getElementById('graphic-template');
	
	fakeData4Model.forEach(function(entry) {
	    var data = entry;
	    var model = new RoundGraphModel(data);
	    var view = new RoundGraphView(model,graphicsWarper);
	    view.build();
	});
 
});
