define(['require','../utils/ColorUtils','./donutGraphic','./trendGraphic'],function(require){

	function RoundGraphView(model,parentElement) {
  	this.model = model;
  	this.data = model.getData();
  	this.parentElement = parentElement;
  	this.color = this.model.getMainColor();
  	this.adjustedColor = ColorUtils.LightenDarken(this.color,-100);
  	this.mainClass = 'graphic-template';
  	this.name = this.model.getTitle().toLowerCase().replace(/ /g,'-');
  }

  RoundGraphView.prototype = {
  	
  	build : function() {

  		var $$ = document;
  		//Warper
  		var warper = $$.createElement('div');
  				warper.classList.add(this.mainClass);
  		
  		//round graphic
  		var graphic = $$.createElement('div');
  				graphic.id = this.mainClass+"-"+this.name;
  				graphic.classList.add('donut-graph');

  		
  		//graphic Header with Title & Total Value
			var header = $$.createElement('div');
					header.classList.add(this.mainClass+'-header');
			
			//graphic title
			var title = $$.createElement('div');
					title.classList.add(this.mainClass+'-title');
					title.innerHTML = this.model.getTitle();
					header.appendChild(title);
			
			//graphic total value
			var totalValue = $$.createElement('div');
					totalValue.classList.add(this.mainClass+'-total-value');
					totalValue.innerHTML = this.model.getTotalValue();
					header.appendChild(totalValue);


			var details = $$.createElement('div');
					details.classList.add(this.mainClass+'-detail', 'clearfix');

					details.appendChild(this.deviceDom('tablet', this.model.getTabletPerc(), this.model.getTabletValue(), this.color ));
					details.appendChild(this.deviceDom('smartphone', this.model.getSmartPhonePerc(), this.model.getSmartPhoneValue(), this.adjustedColor));


  		warper.appendChild(graphic);
  		warper.appendChild(header);
  		warper.appendChild(details);

  		this.parentElement.appendChild(warper);
  		
  		this.donutGraphic();

      if(this.model.getTrend()) {
        this.trendGraphic();
      }
  	},

  	deviceDom : function(name, perc, value, color){
  		
  		var $$ = document;

  		var device = $$.createElement('div');
					device.classList.add(this.mainClass+'-detail-device');

			var deviceName = $$.createElement('div');
					deviceName.classList.add(this.mainClass+'-device-name');
					deviceName.innerHTML = name;
					deviceName.style.color = color;
					device.appendChild(deviceName);

			var devicePerc = $$.createElement('div');
					devicePerc.classList.add(this.mainClass+'-device-perc');
					devicePerc.innerHTML = perc;
					device.appendChild(devicePerc);

			var deviceValue = $$.createElement('div');
					deviceValue.classList.add(this.mainClass+'-device-value');
					deviceValue.innerHTML = value;
					device.appendChild(deviceValue);

  		return device;
  	},

  	//Build donut graphic
  	donutGraphic: function (){
  		
  		var graphData = [
  			{ value: this.model.getSmartPhonePerc(), color: this.adjustedColor	},
  			{	value: this.model.getTabletPerc(), color: this.color }
  		];

  		var DonutGraphic = require("./donutGraphic");
  		var graphic = new DonutGraphic(this.mainClass+"-"+this.name, graphData);
  	},
		
		//Build trend graphic
  	trendGraphic: function(){
  		var TrendGraphic = require("./trendGraphic");
  		var elem = this.mainClass+"-"+this.name;
  		var data = this.model.getTrend();
  		var graphic = new TrendGraphic(elem,data,this.color);
  	}
  };

  return RoundGraphView;
});