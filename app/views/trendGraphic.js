define(['d3.min'],function(){
	
	function TrendGraphic(id,data,color){
		
		this.id = id;
		this.data = data;
		this.margin = {top: 80, right: 0, bottom: 0, left: 0};
		this.width = 130;
    this.height = 130;
    this.color = color;
		this.build();		
	}

	TrendGraphic.prototype = {
		build: function() {

			var x = d3.time.scale().range([0, this.width]);
			var y = d3.scale.linear().range([30, 0]);

			var xAxis = d3.svg.axis().scale(x)
			    .orient("bottom").ticks(5);

			var yAxis = d3.svg.axis().scale(y)
			    .orient("left").ticks(5);

			var area = d3.svg.area()
			    .x(function(d) { return x(d.date); })
			    .y0(this.height)
			    .y1(function(d) { return y(d.value); });

			var valueline = d3.svg.line()
			    .x(function(d) { return x(d.date); })
			    .y(function(d) { return y(d.value); });
			    
			var svg = d3.select("#"+this.id)
			    .append("svg")
			        .attr("class",'trend')
			        .attr("width", this.width )
			        .attr("height", this.height )
			    .append("g")
			        .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");


			this.data.forEach(function(d) {
			  d.date = d.date;
			  d.value = +d.value;
			});
			  
			  //console.log(d) ;
			x.domain(d3.extent(this.data, function(d) { return d.date; }));
			y.domain([0, d3.max(this.data, function(d) { return d.value; })]);
			  
			svg.append("path")
			   .datum(this.data)
			   .attr("class", "trend-svg-area")
			   .attr("style",'fill:'+this.color)
			   .attr("d", area);
			  
			 svg.append("path")
			 	.attr("class", "trend-svg-stroke")
			    .attr("style",'stroke:'+this.color)// Add the valueline path.
			    .attr("d", valueline(this.data));  
			
		}
	};

  return TrendGraphic;

});