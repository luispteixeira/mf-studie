define(['d3.min'],function(){
	/*
	*
	*	data = [{
	*		value : 80 ,
	*   color : '#ff0'
	* },
	*	{
	*		value : 80 ,
	*   color : '#ff0'
	* }]
	*
	*/
	function DonutGraphic(id,data){
		
		this.id = id;
		this.data = data;
		this.width = 180;
    this.height = 180;
   	this.radius = Math.min(this.width, this.height) / 2;
		
		this.build();
	}

	DonutGraphic.prototype = {
		build: function() {
			
			var colorArray = [],
					valueArray = [];

			this.data.forEach(function(d,i){
				colorArray[i] = d.color;
				valueArray[i] = parseInt(d.value);
			});
	
			var arc = d3.svg.arc()
		    		.outerRadius(	this.radius - 10)
		    		.innerRadius(	this.radius - 18);
		    	
		  var pie = d3.layout.pie()
		    		.sort(null);

			var svg = d3.select("#"+this.id).append("svg")
				    .attr("width", this.width)
				    .attr("height", this.height)
				  	.append("g")
				    .attr("transform", "translate(" + this.width / 2 + "," + this.height / 2 + ")");
			  	
			var path = svg.selectAll("path")
			    	.data(pie(valueArray))
			  		.enter().append("path")
			    	.attr("fill", function(d, i) { return colorArray[i]; })
			    	.attr("d", arc);
		}
	};

  return DonutGraphic;

});