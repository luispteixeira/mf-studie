define(['../utils/NumbersExtend'],function () {
	"use strict";

  function RoundGraphModel(data) {
  	this.data = data;
  	//console.log(data);
  }

  RoundGraphModel.prototype = {
  	
  	getData: function(){
  		return this.data;
  	},

  	_formatValue: function (value){
  	/*
		 * @param integer n: length of decimal
		 * @param integer x: length of whole part
		 * @param mixed   s: sections delimiter
		 * @param mixed   c: decimal delimiter
		 */
  		return parseInt(value).format(0, 3, '.', '.');
  	},
  	// Graph Title
  	getTitle: function(){
  		return this.data.title;
  	},
		// Graph Main Value 
  	getTotalValue: function(){
  		return this.data.currency ? this._formatValue(this.data.totalValue) + this.data.currency :this._formatValue(this.data.totalValue);
  	},
		// Graph Value's on Tablet devices
  	getTabletValue: function(){
  		return this.data.currency ?  this._formatValue(this.data.tabletValue) + this.data.currency : this._formatValue(this.data.tabletValue);
  	},
		// Graph Percentage on Tablet
  	getTabletPerc: function(){
  		return (this.data.tabletValue * 100) / this.data.totalValue + "%";
  	},
		// Graph Value's on SmartPhone devices - by calculation total value minus tablet values.
  	getSmartPhoneValue: function(){
  		return this.data.currency ?  this._formatValue(this.data.totalValue-this.data.tabletValue) + this.data.currency : this._formatValue(this.data.totalValue-this.data.tabletValue);
  	},
		// Graph Percentage on SmartPhone devices - by calculation.
  	getSmartPhonePerc: function(){
  		return ( (this.data.totalValue - this.data.tabletValue) * 100) / this.data.totalValue + "%";
  	},

  	getMainColor: function(){
  		return this.data.color;
  	},

  	getTrend: function(){
      return this.data.trend;
  	}
  };

  return RoundGraphModel;

});
